
// MFCDrawRect.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMFCDrawRectApp:
// このクラスの実装については、MFCDrawRect.cpp を参照してください。
//

class CMFCDrawRectApp : public CWinApp
{
public:
	CMFCDrawRectApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CMFCDrawRectApp theApp;