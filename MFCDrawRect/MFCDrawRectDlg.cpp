
// MFCDrawRectDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MFCDrawRect.h"
#include "MFCDrawRectDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCDrawRectDlg ダイアログ


/// https://oshiete.goo.ne.jp/qa/4223875.html


CMFCDrawRectDlg::CMFCDrawRectDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCDrawRectDlg::IDD, pParent)
	, m_bCapture(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	SecureZeroMemory(&m_rect, sizeof(m_rect));
}

void CMFCDrawRectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PC_IMAGE, m_pcImage);
}

BEGIN_MESSAGE_MAP(CMFCDrawRectDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DRAWITEM()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CMFCDrawRectDlg メッセージ ハンドラー

BOOL CMFCDrawRectDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// TODO: 初期化をここに追加します。

	CRect rect;
	m_pcImage.GetClientRect(&rect);
	InvalidateRect(&rect);

	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CMFCDrawRectDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CMFCDrawRectDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMFCDrawRectDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	switch (nIDCtl)
	{
		case IDC_PC_IMAGE:
			{
				CDC* pDC = m_pcImage.GetDC();
				CPen pen;
				pen.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
				CPen* pOldPen = pDC->SelectObject(&pen);

				CBrush brush;
				brush.CreateSolidBrush(RGB(255, 255, 255));
				CBrush* pOldBrush = pDC->SelectObject(&brush);

				CRect rect;
				m_pcImage.GetClientRect(&rect);

				pDC->Rectangle(0, 0, rect.right, rect.bottom);

				pDC->SelectObject(pOldBrush);
				pDC->SelectObject(pOldPen);
			}
			break;

		default:
			CDialogEx::OnDrawItem(nIDCtl, lpDrawItemStruct);
			break;
	}
}


void CMFCDrawRectDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_bCapture)
		return;

	SetCapture();
	m_bCapture = TRUE;

	// マウスのダイアログのクライアント座標をスクリーン座標に変換する。
	ClientToScreen(&point);

	// ピクチャーコントロール上のクライアント座標に変換する。
	m_pcImage.ScreenToClient(&point);
	m_rect.left = point.x;
	m_rect.top = point.y;

	CDialogEx::OnLButtonDown(nFlags, point);
}


void CMFCDrawRectDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	ReleaseCapture();
	m_bCapture = FALSE;
	SecureZeroMemory(&m_rect, sizeof(m_rect));

	CDialogEx::OnLButtonUp(nFlags, point);
}


void CMFCDrawRectDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if (!m_bCapture)
		return;

	// マウスのダイアログのクライアント座標をスクリーン座標に変換する。
	ClientToScreen(&point);

	// ピクチャーコントロール上のクライアント座標に変換する。
	m_pcImage.ScreenToClient(&point);

	if ((point.x == m_rect.right) && (point.y == m_rect.bottom))
		return;

	CDC* pDC = m_pcImage.GetDC();
	HDC hDC = pDC->GetSafeHdc();
	DrawFocusRect(hDC, &m_rect);
	if (m_rect.left < point.x)
	{
		m_rect.right = point.x;
	}
	else
	{
		m_rect.right = m_rect.left;
		m_rect.left = point.x;
	}
	if (m_rect.top < point.y)
	{
		m_rect.bottom = point.y;
	}
	else
	{
		m_rect.bottom = m_rect.top;
		m_rect.top = point.y;
	}
	DrawFocusRect(hDC, &m_rect);
	ReleaseDC(pDC);

	CDialogEx::OnMouseMove(nFlags, point);
}
